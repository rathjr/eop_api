package com.example.eopapi.controller;

import com.example.eopapi.model.Player;
import com.example.eopapi.model.Store;
import com.example.eopapi.payload.ResponseEntity;
import com.example.eopapi.payload.request.PlayerRequest;
import com.example.eopapi.payload.request.StoreRequest;
import com.example.eopapi.service.StoreService;
import com.example.eopapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/store")
public class StoreRestController {
    @Autowired
    StoreService storeService;

    @GetMapping
    public ResponseEntity<List<Store>> findAllStore() {
        try {
//            declare list for store all post then return result
            List<Store> ply = storeService.getAllStore();
            return ResponseEntity
                    .<List<Store>>ok()
                    .setData(ply).setMessage("Stores have been found successfully");
        } catch (Exception ex) {
            return ResponseEntity
                    .<List<Store>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Store> createStore(@RequestBody StoreRequest storeRequest) {
        Store store = new Store();
        store.setName(storeRequest.getName());
        store.setPrice(storeRequest.getPrice());
        store.setDescription(storeRequest.getDescription());
        store.setImages(storeRequest.getImages());
//        declare boolean for call method create if true post well insert success
        boolean isCreate = storeService.create(store);
        if (isCreate)
            return ResponseEntity.<Store>ok().setData(store).setMessage("Store have been create successfully");
        else
            return ResponseEntity.<Store>badRequest().setMessage("Check your Data again");
    }

    @GetMapping("/{id}/view")
    public ResponseEntity<Store> findStoreById(@PathVariable long id) {
//      declare object for call method findByID if found it will return successfully
        Store store = storeService.findStoreByID(id);
        if (store == null) {
            return ResponseEntity
                    .<Store>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
            return ResponseEntity
                    .<Store>ok()
                    .setData(store).setMessage("Player ID " + id + " have been found successfully");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Store> updateStore(@PathVariable Long id,
                                                @RequestBody StoreRequest storeRequest) {
//        find post by id first if found then process update
        Store store = storeService.findStoreByID(id);
        if (store == null) {
            return ResponseEntity
                    .<Store>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
//         set data for update to post
        } else {
            store.setName(storeRequest.getName());
            store.setPrice(storeRequest.getPrice());
            store.setDescription(storeRequest.getDescription());
            store.setImages(storeRequest.getImages());
            boolean isUpdate = storeService.updateStore(store);
            if (isUpdate) {
                return ResponseEntity.<Store>ok().setData(store).setMessage("Store ID " + id + " have been updated successfully");
            } else
                return ResponseEntity.<Store>badRequest().setMessage("Check your Data again");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Store> delete(@PathVariable Long id) {
//       find post by ID first before delete
        Store store = storeService.findStoreByID(id);
        if (store == null) {
            return ResponseEntity
                    .<Store>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
//            if found then it will delete
            boolean isDel = storeService.deleteStore(id);
            if (isDel) {
                return ResponseEntity.<Store>ok().setData(store).setMessage("Store ID" + id + " have been delete successfully");
            } else
                return ResponseEntity.<Store>badRequest().setMessage("Check your Data again");
        }
    }


    @GetMapping("/paging")
    public ResponseEntity<List<Store>> findByPaging(@RequestParam int page, @RequestParam int limit) {
//        declare var as int for call method count
        int countAllStore = storeService.countAllStore();
        Paging paging = new Paging();
//        set page limit TotalCount to paging then return result
        paging.setPage(page);
        paging.setLimit(limit);
        paging.setTotalCount(countAllStore);
        List<Store> store = storeService.findByPaging(paging);
        return ResponseEntity
                .<List<Store>>ok()
                .setData(store).setMetadata(paging);
    }
}
