package com.example.eopapi.controller;

import com.example.eopapi.model.Player;
import com.example.eopapi.payload.ResponseEntity;
import com.example.eopapi.payload.request.PlayerRequest;
import com.example.eopapi.service.PlayerService;
import com.example.eopapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/player")
public class PlayerRestController {
    @Autowired
    PlayerService playerService;

    @GetMapping
    public ResponseEntity<List<Player>> findAllPlayer() {
        try {
//            declare list for store all post then return result
            List<Player> ply = playerService.getAllPlayers();
            return ResponseEntity
                    .<List<Player>>ok()
                    .setData(ply).setMessage("Players have been found successfully");
        } catch (Exception ex) {
            return ResponseEntity
                    .<List<Player>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Player> createPost(@RequestBody PlayerRequest playerRequest) {
        Player player = new Player();
        player.setName(playerRequest.getName());
        player.setBestFoot(playerRequest.getBestFoot());
        player.setHeight(playerRequest.getHeight());
        player.setWeight(playerRequest.getWeight());
        player.setJointed(playerRequest.getJointed());
        player.setNumber(playerRequest.getNumber());
        player.setPlaceOfBirth(playerRequest.getPlaceOfBirth());
        player.setImages(playerRequest.getImages());
        player.setPosition(playerRequest.getPosition());
        player.setTeam(playerRequest.getTeam());
//        declare boolean for call method create if true post well insert success
            boolean isCreate = playerService.create(player);
            if (isCreate)
                return ResponseEntity.<Player>ok().setData(player).setMessage("Post have been create successfully");
            else
                return ResponseEntity.<Player>badRequest().setMessage("Check your Data again");
    }

    @GetMapping("/{id}/view")
    public ResponseEntity<Player> findPlayerById(@PathVariable long id) {
//      declare object for call method findByID if found it will return successfully
        Player player = playerService.findPlayerByID(id);
        if (player == null) {
            return ResponseEntity
                    .<Player>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
            return ResponseEntity
                    .<Player>ok()
                    .setData(player).setMessage("Player ID " + id + " have been found successfully");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Player> updatePlayers(@PathVariable Long id,
                                           @RequestBody PlayerRequest playerRequest) {
//        find post by id first if found then process update
        Player player = playerService.findPlayerByID(id);
        if (player == null) {
            return ResponseEntity
                    .<Player>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
//         set data for update to post
        } else {
            player.setName(playerRequest.getName());
            player.setBestFoot(playerRequest.getBestFoot());
            player.setHeight(playerRequest.getHeight());
            player.setWeight(playerRequest.getWeight());
            player.setJointed(playerRequest.getJointed());
            player.setNumber(playerRequest.getNumber());
            player.setPlaceOfBirth(playerRequest.getPlaceOfBirth());
            player.setImages(playerRequest.getImages());
            player.setPosition(playerRequest.getPosition());
            player.setTeam(playerRequest.getTeam());
            boolean isUpdate = playerService.updatePlayer(player);
            if (isUpdate) {
                return ResponseEntity.<Player>ok().setData(player).setMessage("Player ID " + id + " have been updated successfully");
            } else
                return ResponseEntity.<Player>badRequest().setMessage("Check your Data again");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Player> delete(@PathVariable Long id) {
//       find post by ID first before delete
        Player player = playerService.findPlayerByID(id);
        if (player == null) {
            return ResponseEntity
                    .<Player>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
//            if found then it will delete
            boolean isDel = playerService.deletePlayer(id);
            if (isDel) {
                return ResponseEntity.<Player>ok().setData(player).setMessage("Player ID" + id + " have been delete successfully");
            } else
                return ResponseEntity.<Player>badRequest().setMessage("Check your Data again");
        }
    }

    @GetMapping("/{ps}/findplayersbyposition")
    public ResponseEntity<List<Player>> findAllPlayerByposition(@RequestParam String ps) {
        try {
                List<Player> players = playerService.findAllPlayerbyPosition(ps);
                return ResponseEntity
                        .<List<Player>>ok()
                        .setData(players);
        } catch(Exception ex) {
            return ResponseEntity
                    .<List<Player>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @GetMapping("/paging")
    public ResponseEntity<List<Player>> findByPaging(@RequestParam int page, @RequestParam int limit) {
//        declare var as int for call method count
        int countAllPlayer = playerService.countAllReport();
        Paging paging = new Paging();
//        set page limit TotalCount to paging then return result
        paging.setPage(page);
        paging.setLimit(limit);
        paging.setTotalCount(countAllPlayer);
        List<Player> players = playerService.findByPaging(paging);
        return ResponseEntity
                .<List<Player>>ok()
                .setData(players).setMetadata(paging);
    }


}
