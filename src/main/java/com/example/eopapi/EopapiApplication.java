package com.example.eopapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EopapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EopapiApplication.class, args);
    }

}
