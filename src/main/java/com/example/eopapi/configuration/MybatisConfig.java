package com.example.eopapi.configuration;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

// mark this class as configuration
@Configuration
// map to repo package
@MapperScan("package com.example.eopapi.repository;")
public class MybatisConfig {
    //    inject DataSource via setter
    private DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //    declare datasourceTransactionManager to Bean
    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {

        return new DataSourceTransactionManager(dataSource);

    }

    //    declare sqlSessionFactoryBean to Bean
    @Bean

    public SqlSessionFactoryBean sqlSessionFactoryBean() {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();

        sqlSessionFactoryBean.setDataSource(dataSource);

        return sqlSessionFactoryBean;

    }
}
