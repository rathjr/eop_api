package com.example.eopapi.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

//      mark this class as configuration
@Configuration
public class DatabaseConfig {
    //    declare datasource object to Bean
    @Bean
    public DataSource dataSource() {
//        declare driverManagerDataSource object
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
//        set postgres driver
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
//      set url of connection
        driverManagerDataSource.setUrl("jdbc:postgresql://139.162.45.56:5432/eop");
//      set username
        driverManagerDataSource.setUsername("eop");
//      set password
        driverManagerDataSource.setPassword("191120");
        return driverManagerDataSource;
    }
}
