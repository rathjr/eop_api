package com.example.eopapi.configuration;

import com.example.eopapi.security.UserDetailServiceImp;
import com.example.eopapi.security.jwt.AuthEntryPointJwt;
import com.example.eopapi.security.jwt.JwtToeknfilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@EnableWebSecurity
//  extend WebSecurityConfigurerAdapter for override 3 method security
public class Securityconfig extends WebSecurityConfigurerAdapter {
    //  inject class that need via Autowired
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    private AuthEntryPointJwt unauthorizedHandler;

    @Autowired
    private UserDetailServiceImp userDetailServiceImp;

    //    Add passwordEncode,authenticateManger and jwtTokenFilter to Bean via Bean annotation
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public JwtToeknfilter jwtToeknfilter() {
        return new JwtToeknfilter();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        in memory authentication
        System.out.println("after encode" + passwordEncoder.encode("virus"));
        System.out.println("after encode2" + passwordEncoder.encode("123"));
        auth.userDetailsService(userDetailServiceImp).passwordEncoder(passwordEncoder);
    }

    //    Override this method for secure URL path
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .configurationSource(corsConfigurationSource())
                .and()
                .csrf()
                .disable()
                .exceptionHandling()
                .authenticationEntryPoint(unauthorizedHandler)
                .and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .antMatcher("/api/**")
                .authorizeRequests()
                .antMatchers("/api/player").permitAll()
                .antMatchers("/api/store").permitAll()
                .antMatchers("/api/store/{id}/view").permitAll()
                .antMatchers("/api/player/{id}/view").permitAll()
                .antMatchers("/api/match").permitAll()
                .antMatchers("/api/match/paging").permitAll()
                .antMatchers("/api/match/{id}/view").permitAll()
                .antMatchers("/api/contact/create").permitAll()
//                .antMatchers("/api/users").permitAll()
                .antMatchers("/api/files/upload").permitAll()
                .antMatchers("/api/player/{ps}/findpostbyserviceid").permitAll()
                .antMatchers("/api/auth/**")
                .permitAll()
                .antMatchers("/api/forgetPassword/**")
                .permitAll()
                .anyRequest()
                .authenticated();
        http.addFilterBefore(jwtToeknfilter(), UsernamePasswordAuthenticationFilter.class);
    }

    //  Override this method for ignore resource
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers(
                        "/swagger-ui.html", "/h2-console/**", "/resources/**", "/static/**", "/css/**", "/js/**", "/images/**",
                        "/resources/static/**", "/css/**", "/js/**", "/img/**", "/fonts/**",
                        "/images/**", "/scss/**", "/vendor/**", "/favicon.ico", "/auth/**", "/favicon.png",
                        "/v2/api-docs", "/configuration/ui", "/configuration/security",
                        "/webjars/**", "/swagger-resources/**", "/actuator", "/swagger-ui/**",
                        "/actuator/**", "/swagger-ui/index.html", "/swagger-ui/","/");
    }

    //     add cors interface to bean for allow access API
    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Collections.singletonList("*"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "PATCH","DELETE"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration.applyPermitDefaultValues());
        return source;
    }
}