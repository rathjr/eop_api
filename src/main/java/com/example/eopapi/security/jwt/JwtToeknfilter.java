package com.example.eopapi.security.jwt;
import com.example.eopapi.security.UserDetailImp;
import com.example.eopapi.security.UserDetailServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtToeknfilter extends OncePerRequestFilter {

    @Autowired
    private Jwtutils jwtutils;

    @Autowired
    private UserDetailServiceImp userDetailServiceImp;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        try{
//            check if the request has a valid jwt token.
            String jwt= parseJwt(request);
            if(jwt!=null && jwtutils.verifyJwtToken(jwt)){
                String username=jwtutils.getUsername(jwt);
                UserDetailImp userDetailImp=(UserDetailImp) userDetailServiceImp.loadUserByUsername(username);
                if(username!=null){
                    UsernamePasswordAuthenticationToken authentication =
                            new UsernamePasswordAuthenticationToken(userDetailImp,null ,userDetailImp.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
            filterChain.doFilter(request,response);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    private String parseJwt(HttpServletRequest request){
        String header = request.getHeader("Authorization");
        String prefix = "Bearer ";
        if(StringUtils.hasText(header) && header.startsWith(prefix)){
            return header.substring(prefix.length());
        }
        return null;
    }
}
