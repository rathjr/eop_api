package com.example.eopapi.repository;

import com.example.eopapi.model.Player;
import com.example.eopapi.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PlayerRepository {

    @Select("select * from player")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> getAllPlayers();

    @Insert("INSERT INTO player (name,number,position,placeofbirth,height,weight,jointed,bestfoot,team,images) VALUES (#{name}, #{number},#{position}, #{placeOfBirth},#{height},#{weight},#{jointed},#{bestFoot},#{team},#{images})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Player player);

    @Select("SELECT * FROM player where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    Player findPlayerByID(Long id);

    @Delete("delete from player where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean deletePlayer(Long id);

    @Update("UPDATE player SET name = #{name},number=#{number},position=#{position},placeofbirth=#{placeOfBirth},height=#{height},weight=#{weight},jointed=#{jointed},bestfoot=#{bestFoot},team=#{team},images=#{images} WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean updatePlayer(Player player);


    @Select("SELECT * FROM player where position=#{ps}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> findAllPlayerbyPosition(String ps);



    @Select("SELECT * from player limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "placeofbirth"),
            @Result(property = "bestFoot", column = "bestfoot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> findByPaging(Paging paging);

    @Select("select count(id) from player")
    int countAllReport();
}
