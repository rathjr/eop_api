package com.example.eopapi.repository;

import com.example.eopapi.model.Match;
import com.example.eopapi.model.Player;
import com.example.eopapi.model.Store;
import com.example.eopapi.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StoreRepository {
    @Select("select * from store")
    @Results({
            @Result(property = "createDate", column = "created_date"),
    })
    List<Store> getAllStore();

    @Insert("INSERT INTO store (name,price,description,images) VALUES (#{name}, #{price}, #{description},#{images})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Store store);

    @Select("SELECT * FROM store where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date")
    })
    Store findStoreByID(Long id);
    @Delete("delete from store where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date")
    })
    boolean deleteStore(Long id);
    @Update("UPDATE store SET name = #{name},price=#{price},description=#{description},images=#{images} WHERE id = #{id}")
    @Results({
            @Result(property = "createDate", column = "created_date")
    })
    boolean updateStore(Store store);

    @Select("SELECT * from store limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "createDate", column = "created_date")
    })
    List<Store> findByPaging(Paging paging);

    @Select("select count(id) from store")
    int countAllStore();
}
