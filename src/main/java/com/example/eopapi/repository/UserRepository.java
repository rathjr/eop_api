package com.example.eopapi.repository;

import com.example.eopapi.model.auth.Role;
import com.example.eopapi.model.auth.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;

@Mapper
public interface UserRepository {
    @Select("select * from users where phone_number=#{phoneNumber} and status=true")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
    })
    User findUserByPn(String phoneNumber);

//    @Select("select * from users where firstname=#{firstName} and lastname=#{lastName} and status=true")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "phoneNumber", column = "phone_number"),
//            @Result(property = "firstName", column = "firstname"),
//            @Result(property = "lastName", column = "lastname"),
//            @Result(property = "createDate", column = "created_date"),
//            @Result(property = "idCard", column = "id_card"),
//            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
//    })
//    User findUserByName(String firstName,String lastName);

    @Select("select * from users where status=true")
    @Options(useGeneratedKeys = true, keyProperty = "createDate", keyColumn = "created_date")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
    })
    List<User> findAllUser();

    @Select("SELECT * FROM users_roles ur INNER JOIN roles r ON ur.role_id = r.id WHERE ur.user_id = #{userId}")
    Set<Role> findAllRolesByUserId(int userId);

    @Insert("insert into users(name,password,phone_number) values(#{name},#{password},#{phoneNumber})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    boolean registerUser(User user);

    @Select("select * from users where status=true")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
    })
    List<User> getPhoneNumber();

    @Insert("<script>INSERT INTO users_roles (user_id, role_id) VALUES "
            + "<foreach collection='roles' item='role' separator=','>"
            + "(#{userId}, #{role.id})"
            + "</foreach>"
            + "</script>")
    boolean insertUsersRoles(Long userId, Set<Role> roles);

    default boolean save(User user) {
        if (registerUser(user)) {
            return insertUsersRoles(user.getId(), user.getRoles());
        }
        return false;
    }

    @Update("update users set profile=#{profile} where id=#{id} and status=true")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean updateUser(User user);

    @Select("select * from users where id=#{id} and status=true")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
    })
    User findUserByID(Long id);


    @Update("update users set status=false where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean deleteUsers(Long id);

    @Select("select count(id) from users where status=true")
    int countAllUser();

//    @SelectProvider(type = UserProvider.class, method = "findUserFilter")
//    @Results({
//            @Result(property = "id", column = "id"),
//            @Result(property = "phoneNumber", column = "phone_number"),
//            @Result(property = "firstName", column = "firstname"),
//            @Result(property = "lastName", column = "lastname"),
//            @Result(property = "idCard", column = "id_card"),
//            @Result(property = "createDate", column = "created_date"),
//            @Result(property = "roles", column = "id", many = @Many(select = "findAllRolesByUserId"))
//    })
//    List<User> findUserByFilter(@Param("filter") UserFilter filter, @Param("paging") Paging paging);

}
