package com.example.eopapi.repository;

import com.example.eopapi.model.Match;
import com.example.eopapi.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MatchRepository {
    @Select("select * from match")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "dateofmatch")
    })

    List<Match> getAllMatchs();

    @Insert("INSERT INTO match (name,logo,result,stadium,dateofmatch,ename,elogo,time,type,lresult) VALUES (#{name}, #{logo}, #{result},#{stadium},#{dateOfMatch},#{ename},#{elogo},#{time},#{type},#{lresult})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Match match);

    @Select("SELECT * FROM match where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "dateofmatch")
    })
    Match findMatchByID(Long id);

    @Delete("delete from match where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "dateofmatch")
    })
    boolean deleteMatch(Long id);

    @Update("UPDATE match SET name = #{name},logo=#{logo},result=#{result},stadium=#{stadium},dateofmatch=#{dateOfMatch},ename=#{ename},elogo=#{elogo},time=#{time},type=#{type},lresult=#{lresult} WHERE id = #{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "dateofmatch")
    })
    boolean updateMatch(Match match);

    @Select("SELECT * from match limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "dateofmatch")
    })
    List<Match> findByPaging(Paging paging);

    @Select("select count(id) from match")
    int countAllMatch();
}
