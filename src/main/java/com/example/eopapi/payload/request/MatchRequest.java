package com.example.eopapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MatchRequest {
    private String logo;
    private String result;
    private String name;
    private String stadium;
    private String dateOfMatch;
    private String ename;
    private String elogo;
    private String time;
    private String type;
    private String lresult;
}
