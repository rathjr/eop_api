package com.example.eopapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StoreRequest {
    private String name;
    private String price;
    private String description;
    private String images;
}
