package com.example.eopapi.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Set;

@Setter
@Getter
@ToString
public class RegisterRequest implements Serializable {
    private String name;
    private String password;
    private String phoneNumber;
    private Set<String> roles;
}
