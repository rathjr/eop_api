package com.example.eopapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ContactRequest {
    private String email;
    private String name;
    private String phoneNumber;
}
