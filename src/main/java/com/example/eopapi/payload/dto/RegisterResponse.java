package com.example.eopapi.payload.dto;


import com.example.eopapi.model.auth.Role;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
public class RegisterResponse {
    private int id;
    private String name;
    private String phoneNumber;
    private LocalDate createDate=LocalDate.now();
    private Set<Role> roles = new HashSet<>();
}
