package com.example.eopapi.payload.mapper;

import com.example.eopapi.model.auth.User;
import com.example.eopapi.payload.dto.RegisterResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthMapper {
    RegisterResponse userToRegisterResponse(User user);
    User registerResponseToUser(RegisterResponse response);
}
