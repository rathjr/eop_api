package com.example.eopapi.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
public class ResponseEntity<T> {
    private Status status;
    private int code;
    private String message;
    private LocalDateTime requestTime;
    private T data;
    private boolean success = false;
    private Object metadata;

    public static <T> ResponseEntity<T> badRequest() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        ResponseError error = new ResponseError();
        responseEntity.setStatus(Status.BAD_REQUEST);
        responseEntity.setCode(400);
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> ok() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.OK);
        responseEntity.setSuccess(true);
        responseEntity.setCode(200);
        responseEntity.setMessage("");
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> unauthorized() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.UNAUTHORIZED);
        responseEntity.setCode(401);
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> validationException() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.VALIDATION_EXCEPTION);
        return responseEntity;
    }

    public static <T> ResponseEntity<T> wrongCredentials() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.WRONG_CREDENTIALS);
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> accessDenied() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.ACCESS_DENIED);
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> exception() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
//        responseEntity.setStatus(Status.BAD_REQUEST);
        responseEntity.setCode(400);
        responseEntity.setMessage("");
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> notFound() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.NOT_FOUND);
        responseEntity.setRequestTime(LocalDateTime.now());
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> notAcceptable() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.NOT_ACCEPTABLE);
        responseEntity.setRequestTime(LocalDateTime.now());
        return responseEntity;
    }

    public static <T> ResponseEntity<T> duplicateEntity() {
        ResponseEntity<T> responseEntity = new ResponseEntity<>();
        responseEntity.setStatus(Status.DUPLICATE_ENTITY);
        return responseEntity;
    }

    public void addErrorMsgToResponse(String errorMsg, Exception ex) {
        ResponseError error = new ResponseError()
                .setMessage(ex.getMessage());
    }

    public enum Status {
        OK,
        BAD_REQUEST,
        UNAUTHORIZED,
        NOT_ACCEPTABLE,
        VALIDATION_EXCEPTION,
        EXCEPTION, WRONG_CREDENTIALS, ACCESS_DENIED, NOT_FOUND, DUPLICATE_ENTITY
    }

    @Getter
    @Accessors(chain = true)
    public static class PageMetadata {
        private final int size;
        private final long totalElements;
        private final int totalPages;
        private final int number;

        public PageMetadata(int size, long totalElements, int totalPages, int number) {
            this.size = size;
            this.totalElements = totalElements;
            this.totalPages = totalPages;
            this.number = number;
        }
    }
}
