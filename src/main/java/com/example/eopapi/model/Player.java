package com.example.eopapi.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDate;

@Setter
@Getter
@ToString
public class Player {
    private Long id;
    private String name;
    private int number;
    private String position;
    private String placeOfBirth;
    private String height;
    private String weight;
    private String jointed;
    private String bestFoot;
    private LocalDate createDate=LocalDate.now();
    private String images;
    private String team;
}
