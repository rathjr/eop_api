package com.example.eopapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class Store {
    private Long id;
    private String name;
    private String price;
    private String description;
    private String images;
    private LocalDate createDate=LocalDate.now();

}
