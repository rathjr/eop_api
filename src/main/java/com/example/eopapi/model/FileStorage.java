package com.example.eopapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class    FileStorage {
    private String url;
}
