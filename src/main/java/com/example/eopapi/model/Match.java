package com.example.eopapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class Match {
    private Long id;
    private String logo;
    private String result;
    private String name;
    private String stadium;
    private String dateOfMatch;
    private String ename;
    private String elogo;
    private String time;
    private String type;
    private String lresult;
    private LocalDate createDate=LocalDate.now();
}
