package com.example.eopapi.service;

import com.example.eopapi.model.auth.User;

import java.util.List;

public interface UserService {
    List<User> findAllUser();
    boolean updateUser(User user);
    User findUserByID(Long id);
    boolean deleteUsers(Long id);
}
