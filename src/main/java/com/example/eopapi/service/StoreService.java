package com.example.eopapi.service;

import com.example.eopapi.model.Player;
import com.example.eopapi.model.Store;
import com.example.eopapi.utils.Paging;

import java.util.List;

public interface StoreService {
    List<Store> getAllStore();
    boolean create(Store store);
    Store findStoreByID(Long id);
    boolean deleteStore(Long id);
    boolean updateStore(Store store);
    List<Store> findByPaging(Paging paging);
    int countAllStore();
}
