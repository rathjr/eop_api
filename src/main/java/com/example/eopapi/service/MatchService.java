package com.example.eopapi.service;

import com.example.eopapi.model.Match;
import com.example.eopapi.utils.Paging;

import java.util.List;

public interface MatchService {
    List<Match> getAllMatchs();
    boolean create(Match match);
    Match findMatchByID(Long id);
    boolean deleteMatch(Long id);
    boolean updateMatch(Match match);
    List<Match> findByPaging(Paging paging);
    int countAllMatch();
}
