package com.example.eopapi.service;


import com.example.eopapi.model.auth.User;

import java.util.List;

public interface AuthService {
    boolean registerUser(User user);
    List<User> getPhoneNumber();

}
