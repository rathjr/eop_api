package com.example.eopapi.service;

import com.example.eopapi.model.Contact;

import java.util.List;

public interface ContactService {
    List<Contact> getAllContact();
    boolean create(Contact contact);
    boolean deleteContact(Long id);
    Contact findContactByID(Long id);
}
