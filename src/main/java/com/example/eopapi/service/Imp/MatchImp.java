package com.example.eopapi.service.Imp;

import com.example.eopapi.model.Match;
import com.example.eopapi.repository.MatchRepository;
import com.example.eopapi.service.MatchService;
import com.example.eopapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchImp implements MatchService {
    @Autowired
    MatchRepository matchRepository;
    @Override
    public List<Match> getAllMatchs() {
        return matchRepository.getAllMatchs();
    }

    @Override
    public boolean create(Match match) {
        return matchRepository.create(match);
    }

    @Override
    public Match findMatchByID(Long id) {
        return matchRepository.findMatchByID(id);
    }

    @Override
    public boolean deleteMatch(Long id) {
        return matchRepository.deleteMatch(id);
    }

    @Override
    public boolean updateMatch(Match match) {
        return matchRepository.updateMatch(match);
    }

    @Override
    public List<Match> findByPaging(Paging paging) {
        return matchRepository.findByPaging(paging);
    }

    @Override
    public int countAllMatch() {
        return matchRepository.countAllMatch();
    }
}
