package com.example.eopapi.service.Imp;

import com.example.eopapi.model.auth.User;
import com.example.eopapi.repository.UserRepository;
import com.example.eopapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserImp implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> findAllUser() {
        return userRepository.findAllUser();
    }

    @Override
    public boolean updateUser(User user){
        return userRepository.updateUser(user);
    }

    @Override
    public User findUserByID(Long id) {
        return userRepository.findUserByID(id);
    }

    @Override
    public boolean deleteUsers(Long id) {
        return userRepository.deleteUsers(id);
    }



}
