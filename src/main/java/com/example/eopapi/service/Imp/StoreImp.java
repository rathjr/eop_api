package com.example.eopapi.service.Imp;

import com.example.eopapi.model.Store;
import com.example.eopapi.repository.StoreRepository;
import com.example.eopapi.service.StoreService;
import com.example.eopapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreImp implements StoreService {
    @Autowired
    StoreRepository storeRepository;
    @Override
    public List<Store> getAllStore() {
        return storeRepository.getAllStore();
    }

    @Override
    public boolean create(Store store) {
        return storeRepository.create(store);
    }

    @Override
    public Store findStoreByID(Long id) {
        return storeRepository.findStoreByID(id);
    }

    @Override
    public boolean deleteStore(Long id) {
        return storeRepository.deleteStore(id);
    }

    @Override
    public boolean updateStore(Store store) {
        return storeRepository.updateStore(store);
    }

    @Override
    public List<Store> findByPaging(Paging paging) {
        return storeRepository.findByPaging(paging);
    }

    @Override
    public int countAllStore() {
        return storeRepository.countAllStore();
    }
}
